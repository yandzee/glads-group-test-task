#!/usr/bin/env sh
createdb restaurants
psql restaurants < restaurants.pg.base

pip install flask flask_cors sqlalchemy psycopg2 toml