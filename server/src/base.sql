drop table if exists order_plate cascade;
drop table if exists menu_item_archived cascade;
drop table if exists restaurant_order cascade;
drop table if exists menu_item cascade;
drop table if exists category cascade;
drop table if exists restaurant cascade;
drop table if exists role cascade;
drop table if exists operator cascade;

create table restaurant (
	id serial primary key,
	address varchar
);

create table operator (
	id serial primary key,
	restaurant_id integer not null references restaurant,
	last_name varchar,
	first_name varchar,
	father_name varchar,
	login varchar,
	password varchar,
	last_login timestamptz
);

create table role (
	id serial primary key,
	operator_id integer not null references operator on delete cascade,
	can_order boolean not null default 't',
	can_admin boolean not null default 'f'
);


create table category (
	id serial primary key,
	parent_cat_id integer null references category on delete cascade,
	title varchar
);

create table menu_item (
	id serial primary key,
	cat_id integer references category on delete cascade,
	title varchar,
	price real not null
);

create table menu_item_archived (
	id serial primary key,
	title varchar,
	cat_title varchar null,
	price real not null
);

create table restaurant_order (
	id serial primary key,
	restaurant_id integer not null references restaurant,
	operator_id integer not null references operator,
	order_time timestamp not null default current_timestamp
);

create table order_plate (
	id serial primary key,
	order_id integer not null references restaurant_order on delete cascade,
	item_id integer not null references menu_item_archived
);

insert into restaurant values
	(default, 'Address 1'),
	(default, 'Address 2');

insert into operator values
	(default, 1, 'Туктаров', 'Ренат', 'Рустемович', 'ydz', 'simple', NULL),
	(default, 2, 'Another', 'Name', 'Fathername', 'login', 'pwd', NULL);

insert into role values
	(default, 1, 't', 't'),
	(default, 2, 't', 'f');


insert into category values
	(default, NULL, 'Category 1'),
	(default, NULL, 'Category 2'),
	(default, NULL, 'Category 3');
insert into category values
	(default, 1, 'SubCategory 1'),
	(default, 1, 'SubCategory 2');
insert into category values
	(default, 5, 'SubSubCategory 1'),
	(default, 2, 'SubCategory 1');

insert into menu_item values
	(default, 1, 'Item 1', 200),
	(default, 1, 'Item 2', 295),
	(default, 1, 'Item 3', 175),
	(default, 2, 'Item 4', 150),
	(default, 2, 'Item 5', 300),
	(default, 3, 'Item 6', 130),
	(default, 3, 'Item 7', 560),
	(default, 4, 'Item 8', 570),
	(default, 5, 'Item 9', 410),
	(default, 5, 'Item 10', 500),
	(default, 6, 'Item 11', 330),
	(default, 7, 'Item 12', 330),
	(default, NULL, 'Item 13', 90),
	(default, NULL, 'Item 14', 100);

insert into menu_item_archived values
	(default, 'Item 1', 'Category 1', 200),
	(default, 'Item 2', 'Category 1', 295),
	(default, 'Item 3', 'Category 1', 175),
	(default, 'Item 4', 'Category 2', 150),
	(default, 'Item 5', 'Category 2', 300),
	(default, 'Item 6', 'Category 3', 130),
	(default, 'Item 7', 'Category 3', 560),
	(default, 'Item 8', 'SubCategory 1', 570),
	(default, 'Item 9', 'SubCategory 2', 410),
	(default, 'Item 10', 'SubCategory 2', 500),
	(default, 'Item 11', 'SubSubCategory 1', 330),
	(default, 'Item 12', 'SubCategory 1', 330),
	(default, 'Item 13', NULL, 90),
	(default, 'Item 14', NULL, 100);

insert into restaurant_order values
	(default, 1, 1, current_timestamp),
	(default, 1, 1, current_timestamp + interval '1 hour'),
	(default, 1, 1, current_timestamp + interval '2 hour'),
	(default, 2, 2, current_timestamp + interval '3 hour'),
	(default, 2, 2, current_timestamp + interval '4 hour'),
	(default, 2, 2, current_timestamp + interval '5 hour');

insert into order_plate values
	(default, 1, 1),
	(default, 1, 10),
	(default, 1, 2),
	(default, 2, 11),
	(default, 2, 3),
	(default, 2, 12),
	(default, 2, 4),
	(default, 3, 13),
	(default, 3, 5),
	(default, 3, 1),
	(default, 3, 6),
	(default, 4, 14),
	(default, 5, 7),
	(default, 6, 1),
	(default, 6, 8),
	(default, 6, 1),
	(default, 6, 9);
