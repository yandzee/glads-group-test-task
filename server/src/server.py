#!/usr/bin/env python

from api import set_routes
from datetime import datetime
from db import set_storage
from flask import Flask
from logger import ColoredLogger

import logging
import os
import sys
import time
import toml

CDIR = os.path.dirname(os.path.realpath(__file__))

class App:
	SECRET_KEY = 'H8LeN2idHWDNqvSyb2scshLG6jbIG3wmhzsJzSXiR4rUltKRVmrMM2QqQEpcPlDv'
	CONFIG_FILE = CDIR + '/config.toml'

	def __init__(self):
		print(self.CONFIG_FILE)
		self.logger_setup()
		self.config_setup()
		self.misc_setup()

		self.db_setup()
		# self.test()
		self.api_setup()


	def misc_setup(self):
		general = self.config.get('general')
		mode = general.get('mode', 'development')
		self.debug = True if mode == 'development' else False

		self.logger.info('server running in {} mode'.format(mode))

	def logger_setup(self):
		self.logger = ColoredLogger('app-logger', logging.DEBUG)

	def config_setup(self):
		try:
			self.config = toml.load(self.CONFIG_FILE)
			self.logger.info('config successfully read')
		except Exception as e:
			self.lexit('bad config file: {}'.format(e))

	def db_setup(self):
		try:
			db = set_storage(self, self.config.get('db'))
		except Exception as e:
			self.logger.error(e)
			self.lexit('Cant setup storage')
		self.storage = db

	def api_setup(self):
		flask = Flask('api-module')
		flask.secret_key = self.SECRET_KEY
		try:
			set_routes(self, flask)
			flask.run()
		except Exception as e: #TODO: differ exceptions
			self.lexit('Cant setup api server:\n{}'.format(e))

	""" http handlers """
	def try_login_operator(self, login, password):
		pass

	def try_login_admin(self, login, password):
		return self.storage.find_admin(login, password)

	def current_operator(self, login):
		return self.storage.operator(login)

	def get_menu(self):
		return self.storage.menu()

	def get_stats(self):
		return self.storage.get_stats()

	def get_sales(self):
		return self.storage.get_sales()

	def menu_update(self, menu):
		return self.storage.menu_update(menu)

	def create_menu_item(self, parent_cat_id=None):
		return self.storage.create_menu_item(parent_cat_id)

	def create_menu_category(self, parent_cat_id=None):
		return self.storage.create_menu_category(parent_cat_id)

	def remove_menu_item(self, item_id):
		return self.storage.remove_menu_item(item_id)

	def remove_menu_category(self, cat_id):
		return self.storage.remove_menu_category(cat_id)

	def create_order(self, op, plates):
		return self.storage.create_order(op, plates)

	""" misc """
	def lexit(self, msg):
		self.logger.error(msg)
		sys.exit(1)

if __name__ == '__main__':
	App()