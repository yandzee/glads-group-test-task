import psycopg2

from sqlalchemy import create_engine
from sqlalchemy import desc, Column, ForeignKey, Integer, String, Float, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func

from datetime import datetime

to_dict = lambda r: {c.name: getattr(r, c.name) for c in r.__table__.columns}

Base = declarative_base()

class Restaurant(Base):
	__tablename__ = 'restaurant'

	id = Column(Integer, primary_key = True)
	address = Column(String)

class Operator(Base):
	__tablename__ = 'operator'

	id = Column(Integer, primary_key=True)
	restaurant_id = Column(Integer, ForeignKey('restaurant.id'))
	last_name = Column(String)
	first_name = Column(String)
	father_name = Column(String)
	login = Column(String)
	password = Column(String)
	last_login = Column(DateTime)

	restaurant = relationship('Restaurant')
	role = relationship('Role')
	orders = relationship('Order')

class Role(Base):
	__tablename__ = 'role'

	id = Column(Integer, primary_key=True)
	operator_id = Column(Integer, ForeignKey('operator.id'))
	can_order = Column(Boolean)
	can_admin = Column(Boolean)

class Category(Base):
	__tablename__ = 'category'

	id = Column(Integer, primary_key=True)
	parent_cat_id = Column(Integer, ForeignKey('category.id'))
	title = Column(String)

	items = relationship('MenuItem', back_populates='category', cascade='delete')
	child_cats = relationship('Category', cascade='delete')

class MenuItem(Base):
	__tablename__ = 'menu_item'

	id = Column(Integer, primary_key=True)
	cat_id = Column(Integer, ForeignKey('category.id'))
	title = Column(String)
	price = Column(Float)

	category = relationship('Category', back_populates='items')

class Order(Base):
	__tablename__ = 'restaurant_order'

	id = Column(Integer, primary_key=True)
	restaurant_id = Column(Integer, ForeignKey('restaurant.id'))
	operator_id = Column(Integer, ForeignKey('operator.id'))
	order_time = Column(DateTime, default=datetime.utcnow)

	operator = relationship(Operator, back_populates='orders')
	plates = relationship('OrderPlate', back_populates='order', cascade='save-update')

class ArchivedItem(Base):
	__tablename__ = 'menu_item_archived'

	id = Column(Integer, primary_key=True)
	title = Column(String)
	cat_title = Column(String)
	price = Column(Float)

	plate = relationship('OrderPlate', back_populates='item')

class OrderPlate(Base):
	__tablename__ = 'order_plate'

	id = Column(Integer, primary_key=True)
	order_id = Column(Integer, ForeignKey('restaurant_order.id'))
	item_id = Column(Integer, ForeignKey('menu_item_archived.id'))

	item = relationship(ArchivedItem)
	order = relationship(Order, back_populates='plates')


class Storage:
	def __init__(self, app, db_config, engine):
		self.app = app
		self.logger = app.logger
		self.cfg = db_config
		self.engine = engine

		Session = sessionmaker(bind=engine)
		db = engine.connect()
		self.db = Session(bind=db)


	def operator(self, login):
		op = self.db.query(Operator).filter(Operator.login == login).one_or_none()
		return op and to_dict(op)

	def restaurant(self, op):
		return op.restaurant

	def find_admin(self, login, password):
		admin = self.db.query(Operator).join(Role) \
			.filter(Operator.login == login, Operator.password == password) \
			.filter(Role.can_admin == True) \
			.one_or_none()
		return admin and to_dict(admin)

	def get_stats(self):
		stats = self.db.query(
			ArchivedItem.id,
			ArchivedItem.title,
			ArchivedItem.cat_title,
			func.sum(ArchivedItem.price).over(partition_by=ArchivedItem.id).label('revenue'),
			func.count(ArchivedItem.id).over(partition_by=ArchivedItem.id).label('sales')) \
			.filter(OrderPlate.item_id == ArchivedItem.id) \
			.distinct() \
			.order_by(desc('sales')) \
			.all()
		self.logger.debug(stats)
		return [{
			'title': t[1],
			'cat_title': t[2] or '',
			'revenue': t[3],
			'sales': t[4]
		} for t in stats]

	def get_sales(self):
		sales = self.db.query(
			Order,
			Restaurant.address,
			Operator.first_name,
			Operator.last_name,
			Operator.father_name,
			Order.order_time,
		).join(OrderPlate).filter(
			Order.restaurant_id == Restaurant.id,
			Order.operator_id == Operator.id,
			OrderPlate.order_id == Order.id,
		).all()
		self.logger.debug('sales: {}'.format(sales))

		return [{
			'restaurant': t[1],
			'op': '{} {} {}'.format(t[3], t[2], t[4]),
			'date': t[5].isoformat(),
			'plates': [p.item.title for p in t[0].plates]
		} for t in sales]

	def menu(self):
		items = [to_dict(item) for item in self.db.query(MenuItem).all()]
		categories = [to_dict(cat) for cat in self.db.query(Category).all()]

		root_cats = [cat for cat in categories if cat['parent_cat_id'] is None]
		root_items = [item for item in items if item['cat_id'] is None]
		stack = [cat['id'] for cat in root_cats]
		while stack:
			cat_id = stack.pop()
			cur_cat = next(cat for cat in categories if cat['id'] == cat_id)

			child_cats = [cat for cat in categories if cat['parent_cat_id'] == cat_id]
			stack.extend([cat['id'] for cat in child_cats])
			cur_cat['child_cats'] = child_cats
			cur_cat['child_items'] = [item for item in items if item['cat_id'] == cur_cat['id']]

		return {
			'child_cats': root_cats,
			'child_items': root_items
		}


		for cat in categories:
			cat_items = [item for item in items if item['cat_id'] == cat['id']]
			cat['items'] = cat_items

		return categories

	def flat_items_cats(self, menu_list):
		items = menu_list['child_items']
		categories = []
		cats = menu_list['child_cats']
		while cats:
			cat = cats.pop()
			categories.append({
				'id': cat['id'],
				'title': cat['title']
			})
			cats.extend(cat['child_cats'])
			items.extend(cat['child_items'])

		self.logger.debug(items)
		self.logger.debug(categories)
		return items, categories

	def menu_update(self, menu_list):
		items, cats = self.flat_items_cats(menu_list)
		for item in items:
			db_item = self.db.query(MenuItem).filter(MenuItem.id == item.get('id')).one()
			self.logger.info('menu item(id: {}), title -> {}'.format(item['id'], item['title']))
			db_item.title = item.get('title')
			db_item.price = item.get('price')
			self.db.add(db_item)

		for cat in cats:
			self.logger.info('category(id: {}), title -> {}'.format(cat['id'], cat['title']))
			db_cat = self.db.query(Category).filter(Category.id == cat.get('id')).one()
			db_cat.title = cat.get('title')
			self.db.add(db_cat)

		self.db.commit()
		return True

	def create_menu_item(self, parent_cat_id=None):
		menu_item = MenuItem(title='New Item', cat_id=parent_cat_id, price=0)
		self.db.add(menu_item)
		self.db.commit()
		return to_dict(menu_item)

	def remove_menu_item(self, item_id):
		item = self.db.query(MenuItem).filter(MenuItem.id == item_id).one_or_none()
		if not item:
			return False

		self.db.delete(item)
		self.db.commit()
		return True


	def create_menu_category(self, parent_cat_id=None):
		cat = Category(title='New Category', parent_cat_id=parent_cat_id)
		self.db.add(cat)
		self.db.commit()
		return to_dict(cat)

	def remove_menu_category(self, cat_id):
		cat = self.db.query(Category).filter(Category.id == cat_id).one_or_none()
		if not cat:
			return False
		self.db.delete(cat)
		self.db.commit()
		return True

	def create_order(self, op, plates):
		op = self.db.query(Operator) \
			.filter(Operator.id == op.get('id')) \
			.one_or_none()
		if not op:
			return False
		plates = self.db.query(MenuItem).filter(MenuItem.id.in_(plates)).all()
		order = Order(restaurant_id=op.restaurant.id)
		order.operator = op
		order.plates = []
		for plate in plates:
			order_plate = OrderPlate()
			archived = self.db.query(ArchivedItem) \
				.filter(
					ArchivedItem.title == plate.title,
					ArchivedItem.price == plate.price,
					ArchivedItem.cat_title == plate.category.title) \
				.one_or_none()
			if not archived:
				archived = ArchivedItem(
					title=plate.title,
					cat_title=plate.category.title,
					price=plate.price)
			self.logger.debug('archived: {}'.format(archived.id))
			order_plate.item = archived
			order.plates.append(order_plate)

		# order.plates = [OrderPlate(menu_item_id=p.id, price=p.price) for p in plates]
		self.db.add(order)
		self.db.commit()

		return to_dict(order)

def set_storage(app, db_config):
	basename = db_config.get('basename')
	username = db_config.get('login')
	pwd = db_config.get('password')

	engine = create_engine('postgresql+psycopg2://ydz:@localhost/{}'.format(basename))
	Base.metadata.bind = engine

	return Storage(app, db_config, engine)
