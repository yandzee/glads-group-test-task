from flask import session, redirect, url_for, escape, request, make_response
from flask_cors import CORS

import json

ssid = 'logged_as'

def answer(data = None, error = False):
	result = { 'success': error }
	if type(data) == dict:
		result.update(data)
	elif data:
		result.update({ 'message': data })
	return json.dumps(result)

def error(data = None):
	return answer(data, False)

def success(data = None):
	return answer(data, True)

def not_authorized():
	return session.get(ssid, None) == None

def set_routes(app, handle):
	CORS(handle, supports_credentials=True)

	#XXX: consider as redundant code
	@handle.route('/', methods=['GET'])
	def index():
		login = session.get(ssid, None)
		if login:
			return 'You are logged as {}'.format(logged_as)
		return 'You are not logged in'

	@handle.route('/login', methods=['POST'])
	def login(): #TODO: write last_login field
		if session.get(ssid):
			return success({
				'login_info': 'already logged in',
				'login': session.get(ssid)
			})
		data = request.json
		login = data.get('login', None)
		pwd = data.get('password', None)
		admin = app.try_login_admin(login, pwd)
		app.logger.debug(admin)
		if not admin:
			return error('invalid credentials')

		app.logger.debug('{} is logged in'.format(login))
		session[ssid] = login

		return success({'login': login})


	@handle.route('/logout', methods=['POST'])
	def logout():
		if not_authorized():
			return error('not authorized')

		session.pop(ssid, None)
		return success('successfully logged out')

	@handle.route('/menu', methods=['GET'])
	def menu():
		if not_authorized():
			return error('not authorized')
		menu_list = app.get_menu()
		return success({'menu': menu_list})

	@handle.route('/menu/update', methods=['PUT'])
	def menu_update():
		if not_authorized():
			return error('not authorized')
		data = request.json
		if not data or not data.get('menu'):
			return error('nothing to update');
		r = app.menu_update(data.get('menu'))
		if not r:
			return error('wrong menu');
		return success()

	@handle.route('/menu/create_item', methods=['POST'])
	def menu_create_item():
		if not_authorized():
			return error('not authorized')
		data = request.json
		parent_cat_id = data.get('id')
		item = app.create_menu_item(parent_cat_id)
		if item:
			return success({'menu_item': item})
		else:
			return error('cant create new menu item');

	@handle.route('/menu/remove_item', methods=['POST'])
	def menu_remove_item():
		if not_authorized():
			return error('not authorized')
		data = request.json
		item_id = data.get('id')
		if not item_id:
			return error('select item to remove')
		deleted = app.remove_menu_item(item_id)
		if deleted:
			return success({'deleted': deleted})
		else:
			return error('cant delete this menu item')

	@handle.route('/menu/create_category', methods=['POST'])
	def menu_create_category():
		if not_authorized():
			return error('not authorized')
		data = request.json
		parent_cat_id = data.get('id')
		cat = app.create_menu_category(parent_cat_id)
		if cat:
			return success({'menu_category': cat})
		else:
			return error('cant create new menu category');

	@handle.route('/menu/remove_category', methods=['POST'])
	def menu_remove_category():
		if not_authorized():
			return error('not authorized')

		data = request.json
		cat_id = data.get('id')
		app.logger.debug('api: removing category, json: {}'.format(cat_id))
		if not cat_id:
			return error('select category to remove')
		deleted = app.remove_menu_category(cat_id)
		if deleted:
			return success({'deleted': deleted})
		else:
			return error('cant delete this category')

	@handle.route('/stats', methods=['GET'])
	def get_stats():
		if not_authorized():
			return error('not authorized')
		stats = app.get_stats()
		if stats:
			return success({'stats': stats})
		else:
			return error('cant get stats')

	@handle.route('/sales', methods=['GET'])
	def get_sales():
		if not_authorized():
			return error('not authorized')
		sales = app.get_sales()
		if sales:
			return success({ 'sales': sales })
		else:
			return error('cant get sales')

	@handle.route('/order', methods=['POST'])
	def create_order():
		if not_authorized():
			return error('not authorized')

		op = app.current_operator(session.get(ssid))
		if not op:
			return error('no such operator')
		data = request.json
		plates = data.get('plates')

		app.logger.debug('operator with id {} id making order'.format(op['id']))
		app.logger.debug('data: {}', data)
		r = app.create_order(op, plates)
		if not r:
			return error('cant order')
		else:
			return success({
				'message': 'an order successfully created',
				'order_id': r['id']
			})

