import logging

from datetime import datetime

class ColoredFormatter(logging.Formatter):
	RESET_CLR = "\033[0m"
	COLOR_SEQ = "\033[1;%dm"
	BOLD_SEQ = "\033[1m"

	GRAY, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
	COLORS = {
		'DEBUG': MAGENTA,
		'INFO': GREEN,
		'WARNING': YELLOW,
		'CRITICAL': RED,
		'ERROR': RED
	}

	def __init__(self, msg):
		logging.Formatter.__init__(self, msg)

	def colored(self, part, colorno):
		return self.COLOR_SEQ % (30 + colorno) + part + self.RESET_CLR

	def format(self, record):
		lvlname = record.levelname
		msg = record.msg
		lvl = record.levelno // 10
		time = datetime.fromtimestamp(record.created)
		time = time.strftime('%d.%m.%Y %H:%M:%S')

		time_part = self.colored(time, 7)
		lvl_part = self.colored(lvlname, self.COLORS[lvlname])
		sep = self.colored('|', 0)
		msg_part = msg

		return '{} {} {}: {}'.format(time_part, sep, lvl_part, msg_part)

#TODO: strip msg in logging methods
class ColoredLogger(logging.Logger):
	FORMAT = ""
	def __init__(self, name, lvl):
		logging.Logger.__init__(self, name, lvl)

		formatter = ColoredFormatter(self.FORMAT)
		console = logging.StreamHandler()
		console.setFormatter(formatter)

		self.addHandler(console)