export let range = function*(from, to, step) {
	if (to == null) {
		to = from;
		from = 0;
	}	
	step = step || (from <= to ? 1 : -1);
	if (from >= to && step > 0) {
		step = -step;
	}
	if (from <= to) {
		for (let i = from; i < to; i += step) {
			yield i;
		}
	} else {
		for (let i = from; i > to; i += step) {
			yield i;
		}
	}
};