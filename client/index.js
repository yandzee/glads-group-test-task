import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import App from './components/App'
import Login from './components/Login'
import Admin from './components/Admin'
import Plates from './components/Plates'
import Stats from './components/Stats'
import SalesHistory from './components/SalesHistory'

import { passAdminChecker, needLoginChecker} from './components/auth-checker'

import './statics/styles/font-awesome.min.css'
import './statics/styles/bootstrap.min.css'
import './statics/styles/custom.css'

render((
  <Router history={browserHistory}>
    <Route path='/' component={App}>
        <IndexRoute component={Admin}/>
        <Route path='/login' onEnter={passAdminChecker} component={Login} />
        <Route path='/admin' onEnter={needLoginChecker} component={Admin}>
            <Route path='/plates' component={Plates} />
            <Route path='/sales' component={SalesHistory} />
            <Route path='/report' component={Stats} />
        </Route>
    </Route>
  </Router>
), document.getElementById('app'))
