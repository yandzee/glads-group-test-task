import React from 'react'
import classNames from 'classnames'
import FontAwesome from 'react-fontawesome'
const Icon = FontAwesome;

export default React.createClass({
	getInitialState() {
		return {
			collapsed: true
		};
	},
	toggleCollapse() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	},
	render() {
		let classes = classNames({
			hidden: this.state.collapsed
		});
		let iconName = this.state.collapsed ? 'plus-square-o' : 'minus-square-o';
		return (
			<div>
				<span><Icon className="collapse-icon" name={iconName} onClick={this.toggleCollapse}/></span>
				<input className='flat-input-bold'
					type='text'
					value={this.props.value}
					onChange={this.props.onChange} />
				<div className={classes}>
					{this.props.children}
				</div>
			</div>
		);
	}
});