import React from 'react';
import http from '../modules/http'
import FontAwesome from 'react-fontawesome'
import { Button, Table } from 'react-bootstrap';
const Icon = FontAwesome;

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

export default React.createClass({
	getInitialState() {
		return {
			sales: []
		}
	},
	componentDidMount() {
		http.get(API_URL + '/sales').then(result => {
			result = result.body;
			if (result.success) {
				console.log('sales: ', result.sales);
				this.setState({ sales: result.sales });
			} else {
				this.showError('Cant get sales history');
			}
		});
	},
	render() {
		return (
			<div className="sales-history">
				<h2>Sales</h2>
				<Table stripped condensed>
					<thead>
						<th>Restaurant</th>
						<th>Operator</th>
						<th>Plates</th>
						<th>Date</th>
					</thead>
					<tbody>
						{this.state.sales.map((order, idx) => (
							<tr key={idx.toString()}>
								<td>{order.restaurant}</td>
								<td>{order.op}</td>
								<td>{order.plates.map(plate => (<div>{plate}</div>))}</td>
								<td>{order.date}</td>
							</tr>
						))}
					</tbody>
				</Table>
			</div>
		);
	}
});