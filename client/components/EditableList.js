import React from 'react'
import classNames from 'classnames'
import FontAwesome from 'react-fontawesome'
const Icon = FontAwesome;

import http from '../modules/http'

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

const EditableItem = React.createClass({
	render() {
		return (
			<li>
				<input className='flat-input title-input'
					type='text'
					value={this.props.titleValue}
					onChange={this.props.onTitleChange} />
				<input className='flat-input price-input'
					type='text'
					value={this.props.priceValue}
					onChange={this.props.onPriceChange} />
				<Icon name='times' className="remove-icon" onClick={this.props.remove} />
			</li>
		);
	}
});

const EditableCat = React.createClass({
	getInitialState() {
		return {
			collapsed: true
		};
	},
	toggleCollapse() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	},
	render() {
		let classes = classNames({
			hidden: this.state.collapsed
		});
		let iconName = this.state.collapsed ? 'plus-square-o' : 'minus-square-o';
		return (
			<div>
				<Icon className="collapse-icon" name={iconName} onClick={this.toggleCollapse}/>
				<input className='flat-input-bold'
					type='text'
					value={this.props.value}
					onChange={this.props.onChange} />
				<Icon name='times' className="remove-icon" onClick={this.props.remove} />
				<div className={classes}>
					{this.props.children}
				</div>
			</div>
		);
	}
});

export default React.createClass({
	getInitialState() {
		return {
			menu: {},
			collapsed: true
		}
	},
	componentWillReceiveProps(nextProps) {
		let menu = nextProps.menu;
		this.setState({
			menu
		});
	},
	toggleCollapse() {
		this.setState({
			collapsed: !this.state.collapsed
		});
	},
	createOnChange(path) {
		let setAtPath = v => {
			let parts = path.split('.');
			let last = parts.pop();
			let obj = Object.assign({}, this.state);
			let state = obj;
			for (let part of parts) {
				obj = obj[part];
			}
			obj[last] = v;
			return state;
		};
		return value => {
			let target = value.target;
			if (target != null) {
				if (target.type == 'checkbox') {
					value = target.checked;
				} else if (target.value !== undefined) {
					value = target.value;
				}
			}
			let newState = setAtPath(value);
			this.setState(newState);
		};
	},
	itemsAtPath(path) {
		let parts = path.split('.');
		let obj = Object.assign({}, this.state);
		for (let part of parts) {
			obj = obj[part];
		}
		return obj;
	},
	renderItems(pathToMenu) {
		let itemsPath = pathToMenu + '.child_items';
		let items = this.itemsAtPath(itemsPath);
		if (items == null) return;

		return items.map((item, idx) => {
			let itemPath = itemsPath + '.' + idx;
			let titlePath = itemPath + '.title';
			let pricePath = itemPath + '.price';
			return (
				<div key={idx.toString()}>
					<EditableItem
						titleValue={item.title}
						priceValue={item.price}
						onTitleChange={this.createOnChange(titlePath)}
						onPriceChange={this.createOnChange(pricePath)}
						remove={_ => this.removeItem(itemPath)}
					/>
				</div>);
		});
	},
	renderCats(pathToMenu) {
		let catsPath = pathToMenu + '.child_cats';
		let cats = this.itemsAtPath(catsPath);
		if (cats == null) return;

		return cats.map((cat, idx) => {
			let catPath = catsPath + '.' + idx;
			let valuePath = catPath + '.title';
			return (
				<EditableCat
					value={cat.title}
					onChange={this.createOnChange(valuePath)}
					remove={_ => this.removeCat(catPath)}>
					{this.renderMenu(catsPath + '.' + idx)}
				</EditableCat>
			);
		});
	},
	renderMenu(pathToMenu) {
		let itemElements = this.renderItems(pathToMenu);
		let catsElements = this.renderCats(pathToMenu);

		return (
			<ul>
				{catsElements}
				<li className="maintain">
					<a href='#' onClick={_ => this.addCat(pathToMenu)}>add category</a>
				</li>
				{itemElements}
				<li className="maintain">
					<a href='#' onClick={_ => this.addItem(pathToMenu)}>add item</a>
				</li>
			</ul>
		);
	},
	addItem(pathToMenu) {
		let parentCat = this.itemsAtPath(pathToMenu);
		this.props.addItem(parentCat.id);
	},
	removeItem(pathToMenu) {
		let item = this.itemsAtPath(pathToMenu);
		this.props.removeItem(item.id);
	},
	addCat(pathToMenu) {
		let parentCat = this.itemsAtPath(pathToMenu);
		this.props.addCat(parentCat.id);
	},
	removeCat(pathToMenu) {
		let cat = this.itemsAtPath(pathToMenu);
		this.props.removeCat(cat.id);
	},
	render() {
		let classes = classNames({
			hidden: this.state.collapsed
		});
		let collapseIcon = this.state.collapsed ? 'plus-square-o' : 'minus-square-o';
		let elements = this.renderMenu('menu');
		return elements;
	}
});