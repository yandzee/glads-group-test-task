function authorized() {
	return localStorage.getItem('authorized') === '1';
}

export function needLoginChecker(nextState, replace, pass) {
	if (!authorized()) {
		replace('/login');
	}
	pass();
}

export function passAdminChecker(nextState, replace, pass) {
	if (authorized()) {
		replace('/admin');
	}
	pass();
}