import React from 'react'
import { Link, browserHistory } from 'react-router'
import {Form, FormGroup, FormControl, Checkbox, Button, Col, ControlLabel} from 'react-bootstrap';

import http from '../modules/http'

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

export default React.createClass({
	getInitialState() {
		return {
			username: localStorage.getItem('username')
		};
	},
	componentDidMount() {
		http.get
	},
	logout() {
		http.post(API_URL + '/logout').then(result => {
			result = result.body;
			if (result.success) {
				localStorage.removeItem('authorized');
				browserHistory.push('/login');
			}
		});
	},
	createOnChange(path) {
		let setAtPath = v => {
			let parts = path.split('.');
			let last = parts.pop();
			let obj = Object.assign({}, this.state);
			let state = obj;
			for (let part of parts) {
				obj = obj[part];
			}
			obj[last] = v;
			return state;
		};
		return value => {
			let target = value.target;
			if (target != null) {
				if (target.type == 'checkbox') {
					value = target.checked;
				} else if (target.value !== undefined) {
					value = target.value;
				}
			}
			let newState = setAtPath(value);
			this.setState(newState);
		};
	},
	render() {
		return (
			<div className="admin-layout">
				<div className="admin-menu">
					<div>
						Logged as <strong>{this.state.username}</strong>&nbsp;
						(<a href='#' onClick={this.logout}>Logout</a>)
					</div><br />
					<div><ul>
						<li><Link to='/plates'>Товары и категории</Link></li>
						<li><Link to='/sales'>История продаж</Link></li>
						<li><Link to='/report'>Отчет</Link></li>
					</ul></div>
				</div>
				<div className="admin-content">
					{this.props.children}
				</div>
			</div>
		);
	}
})
