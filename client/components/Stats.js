import React from 'react';
import http from '../modules/http'
import FontAwesome from 'react-fontawesome'
import { Button, Table } from 'react-bootstrap';
const Icon = FontAwesome;

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

export default React.createClass({
	getInitialState() {
		return {
			stats: []
		};
	},
	showError(msg) {
		console.error(msg);
	},
	componentDidMount() {
		http.get(API_URL + '/stats').then(result => {
			result = result.body;
			if (result.success) {
				console.log('stats: ', result.stats);
				this.setState({ stats: result.stats });
			} else {
				this.showError('Cant get stats');
			}
		});
	},
	render() {
		return (
			<div className="stats-table">
				<h2>Stats</h2>
				<Table stripped condensed>
					<thead>
						<th>Product</th>
						<th>Category</th>
						<th>Revenue</th>
						<th>Sales</th>
					</thead>
					<tbody>
						{this.state.stats.map((product, idx) => (
							<tr key={idx.toString()}>
								<td>{product.title}</td>
								<td>{product.cat_title}</td>
								<td>{product.revenue}</td>
								<td>{product.sales}</td>
							</tr>
						))}
					</tbody>
				</Table>
			</div>
		);
	}
});