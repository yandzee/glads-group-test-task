import React from 'react';
import http from '../modules/http'
import EditableMenu from './EditableMenu'
import FontAwesome from 'react-fontawesome'
import { Button } from 'react-bootstrap';
const Icon = FontAwesome;

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

export default React.createClass({
	getInitialState() {
		return {
			menu: {}
		}
	},
	fetchMenu() {
		return http.get(API_URL + '/menu').then(result => {
			console.log(result);
			result = result.body;
			if (result.success) {
				return result.menu;
			} else {
				throw new Error('Cant fetch menu');
			}
		});
	},
	refreshMenu() {
		this.fetchMenu().then(menu => {
			this.setState({ menu });
		}).catch(err => {
			this.showError('cant fetch menu: ', err);
		});
	},
	componentDidMount() {
		this.refreshMenu();
	},
	showError(msg) {
		console.error(msg);
	},
	notify(msg) {
		console.info(msg);
	},
	renderChildItems(items) {
		if (items == null) {
			return;
		}
		return items.map((item, idx) => (
			<li key={idx.toString()}>{item.title}</li>
		));
	},
	updateMenu(e) {
		http.put(API_URL + '/menu/update', {
			menu: this.state.menu
		}).then(result => {
			result = result.body;
			if (result.success) {
				this.notify('Menu successfully updated');
			} else {
				this.showError('En error occured when trying to update menu');
			}
		});
	},
	addCat(parentCatId) {
		console.log('addCat: ', parentCatId);
		http.post(API_URL + '/menu/create_category', {
			id: parentCatId
		}).then(result => {
			result = result.body;
			if (result.success) {
				this.refreshMenu()
			} else {
				this.showError('Cant add category');
			}
		});
	},
	addItem(parentCatId) {
		http.post(API_URL + '/menu/create_item', {
			id: parentCatId
		}).then(result => {
			result = result.body;
			if (result.success) {
				this.refreshMenu()
			} else {
				this.showError('Cant add item');
			}
		});
	},
	removeCat(catId) {
		http.post(API_URL + '/menu/remove_category', {
			id: catId
		}).then(result => {
			result = result.body;
			if (result.success) {
				this.refreshMenu();
			} else {
				this.showError('Cant delete category ' + catId);
			}
		})
	},
	removeItem(itemId) {
		http.post(API_URL + '/menu/remove_item', {
			id: itemId
		}).then(result => {
			result = result.body;
			if (result.success) {
				this.refreshMenu()
			} else {
				this.showError('Cant remove item ' + itemId);
			}
		});
	},
	createOnChange(path) {
		let setAtPath = v => {
			let parts = path.split('.');
			let last = parts.pop();
			let obj = Object.assign({}, this.state);
			let state = obj;
			for (let part of parts) {
				obj = obj[part];
			}
			obj[last] = v;
			return state;
		};
		return value => {
			let target = value.target;
			if (target != null) {
				if (target.type == 'checkbox') {
					value = target.checked;
				} else if (target.value !== undefined) {
					value = target.value;
				}
			}
			let newState = setAtPath(value);
			this.setState(newState);
		};
	},
	renderCategories() {
		let menu = Object.assign({}, this.state.menu);
		console.log('menu: ', menu);
		return (
			<EditableMenu
				label='Plates'
				menu={menu}
				addCat={this.addCat}
				addItem={this.addItem}
				removeCat={this.removeCat}
				removeItem={this.removeItem}
			/>
		);
	},
	render() {
		return (
			<div>
				<h2>Plates</h2>
				<div>{this.renderCategories()}</div>
				<div>
					<Button onClick={this.updateMenu} bsStyle='primary'>
						Save
					</Button>
				</div>
			</div>
		);
	}
});