import React from 'react'
import { Link, browserHistory } from 'react-router'
import {Alert, Form, FormGroup, FormControl, Checkbox, Button, Col, ControlLabel} from 'react-bootstrap';

import http from '../modules/http'

const API_URL =
	document.location.protocol + '//' +
	document.location.hostname + ':5000';

export default React.createClass({
	getInitialState() {
		return {
			login: '',
			password: '',
			rememberMe: false,
			errorMessage: null
		};
	},
	handleSubmit(e) {
		e.preventDefault();
		let loginData = this.state;
		console.log(loginData);
		http.post(API_URL + '/login', loginData).then(d => {
			console.log(d);
			let data = d.body;
			if (data.success) {
				this.localAuthorize(data);
				this.redirectToAdmin();
			} else {
				this.showError(data.message);
			}
		});
	},
	localAuthorize(credentials) {
		localStorage.setItem('authorized', 1);
		localStorage.setItem('username', credentials.login)
	},
	redirectToAdmin() {
		browserHistory.push('/admin')
	},
	showError() {
		this.setState({
			errorMessage: 'Invalid credentials'
		});
		setTimeout(_ => {
			this.setState({
				errorMessage: null
			});
		}, 2000);
	},
	createOnChange(path) {
		let setAtPath = v => {
			let parts = path.split('.');
			let last = parts.pop();
			let obj = Object.assign({}, this.state);
			let state = obj;
			for (let part of parts) {
				obj = obj[part];
			}
			obj[last] = v;
			return state;
		};
		return value => {
			let target = value.target;
			if (target != null) {
				if (target.type == 'checkbox') {
					value = target.checked;
				} else if (target.value !== undefined) {
					value = target.value;
				}
			}
			let newState = setAtPath(value);
			this.setState(newState);
		};
	},
	render() {
		return (
			<div className="login-layout">
				<div className="login-form">
					<Form horizontal>
						<FormGroup>
							<h1>B-Burgers panel</h1>
						</FormGroup>
						<FormGroup controlId="formHorizontalEmail">
							<Col componentClass={ControlLabel} sm={2}>
								Login
							</Col>
							<Col sm={10}>
								<FormControl
									type="text"
									placeholder="Login"
									value={this.state.login}
									onChange={this.createOnChange('login')}
								/>
							</Col>
						</FormGroup>

						<FormGroup controlId="formHorizontalPassword">
							<Col componentClass={ControlLabel} sm={2}>
								Password
							</Col>
							<Col sm={10}>
								<FormControl
									type="password"
									placeholder="Password"
									value={this.state.password}
									onChange={this.createOnChange('password')}
								/>
							</Col>
						</FormGroup>

						<FormGroup>
							<Col smOffset={2} sm={10}>
								<Checkbox
									checked={this.state.rememberMe}
									onChange={this.createOnChange('rememberMe')}
								>Remember me</Checkbox>
							</Col>
						</FormGroup>

						<FormGroup>
							<Col smOffset={2} sm={10}>
								<Button bsStyle="primary" onClick={this.handleSubmit} data-toggle="buttons">
									Sign in
								</Button>
							</Col>
						</FormGroup>
						<FormGroup>
							<Col smOffset={0} sm={12}>
								<Alert bsStyle="danger" hidden={this.state.errorMessage == null}>
									{this.state.errorMessage}
								</Alert>
							</Col>
						</FormGroup>
					</Form>
				</div>
			</div>
		);
	}
})
